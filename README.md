# Mortar

A two-way Morse code translator

### Features

- Automatically detects if input is text or Morse code
- Translation from text to Morse code
- Translation from Morse code to text
- Perfect whitespace keeping (doesn't screw with it)
- Beautiful red errors when you give it characters/codes not included in its 1,2 KB dictionary
- Runs instantly on a 2005 Eee PC
- Almost no memory footprint
- Eco-friendly
- Written in C++ `<3`
- Can translate the dollar sign
- Made with love, Disco Polo, Rock, Alestorm and lots of tea

### Installation

Download the source code and build it with the command `c++ -o mortar main.cpp -std=c++17`. The executable is ready to run, splonk it wherever you want.

### Usage
Here's some examples:

---

##### 1
Command: `mortar "Hello, world"`

Output: `.... . .-.. .-.. --- --..--  .-- --- .-. .-.. -..`

---

##### 2
Command: `mortar ".... . .-.. .-.. --- --..--  .-- --- .-. .-.. -.."`

Output: `HELLO, WORLD`

---

##### 3
You can also supply multiple strings to translate: `./mortar Rum "-... . . .-." quests "-- . .- -.."`

Output:
```
.-. ..- -- 
BEER
--.- ..- . ... - ... 
MEAD
```

----

### Its dictionary

```
    {"A",  ".-"},
    {"B",  "-..."},
    {"C",  "-.-."},
    {"D",  "-.."},
    {"E",  "."},
    {"F",  "..-."},
    {"G",  "--."},
    {"H",  "...."},
    {"I",  ".."},
    {"J",  ".---"},
    {"K",  "-.-"},
    {"L",  ".-.."},
    {"M",  "--"},
    {"N",  "-."},
    {"O",  "---"},
    {"P",  ".--."},
    {"Q",  "--.-"},
    {"R",  ".-."},
    {"S",  "..."},
    {"T",  "-"},
    {"U",  "..-"},
    {"V",  "...-"},
    {"W",  ".--"},
    {"X",  "-..-"},
    {"Y",  "-.--"},
    {"Z",  "--.."},

    {"1",  ".---"},
    {"2",  "..---"},
    {"3",  "...--"},
    {"4",  "....-"},
    {"5",  "....."},
    {"6",  "-...."},
    {"7",  "--..."},
    {"8",  "---.."},
    {"9",  "----."},
    {"0",  "-----"},

    {".",  ".-.-.-"},
    {",",  "--..--"},
    {"?",  "..--.."},
    {"\'", ".----."},
    {"!",  "-.-.--"},
    {"/",  "-..-."},
    {"(",  "-.--."},
    {")",  "-.--.-"},
    {"&",  ".-..."},
    {":",  "---..."},
    {";",  "-.-.-."},
    {"=",  "-...-"},
    {"+",  ".-.-."},
    {"-",  "-....-"},
    {"_",  "..--.-"},
    {"\"", ".-..-."},
    {"$",  "..._.._"}
```

_Note that you don't need to capitalise letters, since, thanks to the magic of 32, the program does it automatically_
