#include <iostream>
#include <vector>
#include "stringIsText.h"
#include "textToMorse.h"
#include "morseToText.h"
#include "capitalise.h"

int main (int argc, char *argv[])
{
  std::vector<std::string> untranslatedTexts;
  untranslatedTexts.reserve ((unsigned) argc - 1);
  for (unsigned i = 1; i < argc; i++)
    {
      untranslatedTexts.push_back (argv[i]);
      capitalise (untranslatedTexts[i - 1]);
    }

  for (auto &untranslatedText : untranslatedTexts)
    {
      if (stringIsText (untranslatedText))
        {
          std::cout << textToMorse (untranslatedText) << std::endl;
        }
      else
        {
          std::cout << morseToText (untranslatedText) << std::endl;
        }
    }

  return 0;
}
