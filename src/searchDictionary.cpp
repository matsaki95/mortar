#include "dictionary.h"

std::string searchDictionary (const char charToTranslate)
{
  for (auto &&[text, morseCode] : dictionary)
    {
      if (text[0] == charToTranslate)
        {
          return morseCode;
        }
    }

  //Getting here means nothing was found
  std::cout << "\033[91mError: \033[31;1mCharacter \'" << charToTranslate << "\' can't be translated.\033[0m" << std::endl;
  exit (1);
}

std::string searchDictionary (const std::string &codeToTranslate)
{
  for (auto &&[text, morseCode] : dictionary)
    {
      if (morseCode == codeToTranslate)
        {
          return text;
        }
    }

  //Getting here means nothing was found
  std::cout << "\033[91mError: \033[31;1mCode \"" << codeToTranslate << "\" can't be translated.\033[0m" << std::endl;
  exit (1);
}
