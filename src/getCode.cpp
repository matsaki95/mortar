std::string getCode (const std::string &source, unsigned &i)
{
  std::string code;
  while (!whitespace (source[i]) && i < source.length ())
    {
      code += source[i];
      i++;
    }
  return code;
}
