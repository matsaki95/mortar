#include <string>
#include "whitespace.h"

bool stringIsText (const std::string &stringInQuestion)
{
  for (auto &character : stringInQuestion)
    {
      if (character != '.' && character != '-' && !whitespace (character))
        {
          return true;
        }
    }

  return false;
}
