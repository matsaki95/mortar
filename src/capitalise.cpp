#include <string>

void capitalise (std::string &stringToCapitalise)
{
  for (auto &letter : stringToCapitalise)
    {
      if ('a' <= letter && letter <= 'z')
        {
          letter -= 32;
        }
    }
}
